# Wireguard docker

wireguard server with ui for configuration and pihole to avoid advertising

## mandatory variables

* wireguard_ui_username
* wireguard_ui_password
* pihole_admin_password
* wireguard_domain
